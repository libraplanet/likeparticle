'参考サイト
'http://d.hatena.ne.jp/runicalp/20081117


'カレントにフォルダを作って解凍します

'オブジェクトを作ります
set fso = WScript.CreateObject("Scripting.FileSystemObject")

'とりあえず解凍する対象は一つだけにします。
If WScript.Arguments.Count < 1 Then
	WScript.Echo("ファイルをドロップしてくだせぇ")
	WScript.Quit
ElseIf WScript.Arguments.Count > 1 Then
	WScript.Echo("1個にしてください･･･")
	WScript.Quit
End If
File = WScript.Arguments(0)

If LCase(fso.GetExtensionName(File)) <> "zip" Then
	WScript.Echo("ZIP形式のみ解凍できます！")
	WScript.Quit()
End If

'フォルダの名前を決めます
ExtractFolder = fso.GetParentFolderName(File) & "\" & fso.GetBaseName(File)

'なかったら作ります
If Not fso.FolderExists(ExtractFolder) Then
	fso.CreateFolder(ExtractFolder)
End If

'解凍！
With CreateObject("Shell.Application")
	.NameSpace(ExtractFolder).CopyHere .NameSpace(File).Items
End With

