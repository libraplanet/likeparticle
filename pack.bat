@SETLOCAL
@SET _COMP_MODE=9
@SET _TMP_FILE=.tmp.tmp
@SET _OUT_FILE=LikeParticle.zip
DEL /F %_TMP_FILE%
7za a -tzip -mx=%_COMP_MODE% %_TMP_FILE% ./LikeParticle -r -x!./bin/ -x!./obj/
7za a -tzip -mx=%_COMP_MODE% %_TMP_FILE% ./scrap -r
7za a -tzip -mx=%_COMP_MODE% %_TMP_FILE% ./*.* -x!./.backup/ -x!./lib/ -x!%_TMP_FILE% -x!%_OUT_FILE%
MOVE /Y %_TMP_FILE% %_OUT_FILE%
@ENDLOCAL