﻿using System;
using System.Text;
using Drawing = System.Drawing;

namespace LikeParticle.Content
{
    /// <summary>
    /// 描画機能をGDI(.NET FrameworkのGraphics)で実装したクラス
    /// </summary>
    public class GdiGraphics : IGraphics
    {
        static Drawing.Font font;

        /// <summary>
        /// 基となるGraphics
        /// </summary>
        Drawing.Graphics g;

        /// <summary>
        /// 静的初期化メソッド
        /// </summary>
        static GdiGraphics()
        {
            font = new Drawing.Font("ＭＳ ゴシック", 12);
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="g"></param>
        public GdiGraphics(Drawing.Graphics g)
        {
            this.g = g;
        }

        /// <summary>
        /// 文字列の描画
        /// </summary>
        /// <param name="g"></param>
        /// <param name="str"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="c"></param>
        public void DrawString(string str, float x, float y, Drawing.Color c)
        {
            g.DrawString(str, font, new Drawing.SolidBrush(c), x, y);
        }

        /// <summary>
        /// 矩形の塗りつぶしの描画
        /// </summary>
        /// <param name="g"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <param name="c"></param>
        public void FillRect(float x, float y, float w, float h, Drawing.Color c)
        {
            g.FillRectangle(new Drawing.SolidBrush(c), new Drawing.Rectangle((int)x, (int)y, (int)w, (int)h));
        }

        /// <summary>
        /// 座標を中心に、それぞれの半径の円の描画
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="rw"></param>
        /// <param name="rh"></param>
        /// <param name="c"></param>
        public void FillCircleCenter(float x, float y, float rw, float rh, Drawing.Color c)
        {
            FillCircleBox(x - rw, y - rh, rw * 2, rh * 2, c); 
        }

        /// <summary>
        /// 矩形による内接する円の塗りつぶしを描画
        /// </summary>
        /// <param name="g"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <param name="c"></param>
        public void FillCircleBox(float x, float y, float w, float h, Drawing.Color c)
        {
            g.FillEllipse(new Drawing.SolidBrush(c), new Drawing.Rectangle((int)x, (int)y, (int)w, (int)h));
        }
    }
}
