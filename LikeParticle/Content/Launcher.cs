﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LikeParticle.Content
{
    /// <summary>
    /// フォームを起動させるLauncherクラス
    /// </summary>
    public partial class Launcher : Form
    {
        Game game;
        ThreadLauncher tl = null;
        List<Thread> thList = new List<Thread>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="game"></param>
        public Launcher(Game game)
        {
            InitializeComponent();

            //instance
            {
                this.game = game;
            }

            //events
            {
                this.Disposed += new EventHandler(Launcher_Disposed);
            }
            //timer
            {
                System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                timer.Interval = 100;
                timer.Tick += new EventHandler(timer_Tick);
                timer.Start();
            }
        }

        /// <summary>
        /// 定期更新
        /// 今現在activeなフォームの描画結果を、
        /// mini monitorに描画します。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void timer_Tick(object sender, EventArgs e)
        {
            Image res = null;
            try
            {
                if (IsThreadLauncher())
                {
                    lock (game)
                    {
                        CustomForm.ICustomForm form = tl.form;
                        if (form != null)
                        {
                            Image img = form.GetImage();
                            if (img != null)
                            {
                                res = new Bitmap(img, pictureBoxMiniMonitor.Size);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                res = null;
            }
            finally
            {
                pictureBoxMiniMonitor.Image = res;
            }
        }

        /// <summary>
        /// GdiFormボタンのクリックイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonGdiForm_Click(object sender, EventArgs e)
        {
            Launch(ThreadLauncher.FORM_MODE.GDIFORM);
        }

        /// <summary>
        /// SdlFormボタンのクリックイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSdlButton_Click(object sender, EventArgs e)
        {
            Launch(ThreadLauncher.FORM_MODE.SDLFORM);
        }

        /// <summary>
        /// レイアウトの更新
        /// ボタンの押下可否を設定します。
        /// </summary>
        private void UpdateLayout()
        {
            lock (this)
            {
                if (tl == null)
                {
                    buttonGdiForm.Enabled = true;
                    buttonSdlForm.Enabled = true;
                }
                else
                {
                    buttonGdiForm.Enabled = tl.mode != ThreadLauncher.FORM_MODE.GDIFORM;
                    buttonSdlForm.Enabled = tl.mode != ThreadLauncher.FORM_MODE.SDLFORM;
                }
            }
        }

        /// <summary>
        /// Formの起動
        /// </summary>
        /// <param name="mode"></param>
        void Launch(ThreadLauncher.FORM_MODE mode)
        {
            if (tl == null)
            {
                new ThreadLauncher(this, mode).Start();
            }
            else
            {
                if (tl.mode != mode)
                {
                    tl.Close();
                    while(IsThreadLauncher());//.th.Join();
                    RegisterThreadLauncher(null);
                    UpdateLayout();
                    new ThreadLauncher(this, mode).Start();
                }
            }
        }

        /// <summary>
        /// 今アクティブなThreadLauncherを登録します。
        /// nullを指定で登録解除。
        /// </summary>
        /// <param name="tl"></param>
        void RegisterThreadLauncher(ThreadLauncher tl)
        {
            lock (this)
            {
                this.tl = tl;
            }
        }

        /// <summary>
        /// 保護処置用にthreadを収集します。
        /// addの可否で追加/削除を行います。
        /// </summary>
        /// <param name="th"></param>
        void CollectThread(Thread th, bool add)
        {
            lock (this)
            {
                if (add)
                {
                    thList.Add(th);
                }
                else
                {
                    thList.Remove(th);
                }
            }
        }

        /// <summary>
        /// いまThreadLauncherが登録されているかを取得します。
        /// </summary>
        /// <returns></returns>
        bool IsThreadLauncher()
        {
            lock (this)
            {
                return (this.tl != null);
            }
        }

        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Launcher_Disposed(object sender, EventArgs e)
        {
            //収集したthreadを全て破棄
            {
                while (thList.Count > 0)
                {
                    thList[0].Abort();
                    thList[0].Join();
                    thList.RemoveAt(0);
                }
            }
        }

        /// <summary>
        /// 起動したCustomForm.ICustomFormのTick中からのコールバック
        /// </summary>
        void FormTickCallBack(CustomForm.ICustomForm form)
        {
            //ウィンドウをランチャーの右につける
            Lib.Windows.Win32.SetWindowPos(form.GetHWND(), IntPtr.Zero, this.Bounds.Right, this.Bounds.Top, 0, 0, Lib.Windows.Win32.SWP_NOSIZE);
        }

        /// <summary>
        /// 指定FoamをThreadで起動する為の管理クラス。
        /// </summary>
        class ThreadLauncher
        {
            /// <summary>
            /// 起動モード
            /// </summary>
            public enum FORM_MODE
            {
                /// <summary>
                /// GdiFormで起動
                /// </summary>
                GDIFORM,
                /// <summary>
                /// SdlFormで起動
                /// </summary>
                SDLFORM,
            };

            /// <summary>
            /// Launcher.UpdateLayout()をInvokeから呼ぶためのdelegate
            /// </summary>
            delegate void delegateInvokeUpdateLayout();

            /// <summary>
            /// ICustomForm.Close()をInvokeから呼ぶためのdelegate
            /// </summary>
            delegate void delegateInvokeClose();

            /// <summary>
            /// 親のLauncher
            /// (Javaのエンクロージャーが欲しいところ・・・)
            /// </summary>
            Launcher launcher;

            public FORM_MODE mode;
            public Thread th = null;
            public CustomForm.ICustomForm form;

            /// <summary>
            /// コンストラクタ
            /// </summary>
            /// <param name="mode"></param>
            public ThreadLauncher(Launcher launcher, FORM_MODE mode)
            {
                this.launcher = launcher;
                this.mode = mode;
            }

            /// <summary>
            /// 開始
            /// </summary>
            public void Start()
            {
                if (th == null)
                {
                    th = new Thread(Run);
                    th.Start();
                }
            }

            /// <summary>
            /// 終了
            /// 親のLauncherから終了を行う用。
            /// </summary>
            public void Close()
            {
                CustomForm.ICustomForm form = null;

                do
                {
                    form = this.form;
                }
                while (form == null);
                launcher.Invoke(new delegateInvokeClose(form.Close));
            }

            /// <summary>
            /// Threadの実行部分
            /// </summary>
            void Run()
            {
                //collect thread
                {
                    launcher.CollectThread(th, true);
                }
                if (!launcher.IsThreadLauncher())
                {
                    //lock
                    {
                        launcher.RegisterThreadLauncher(this);
                        launcher.Invoke(new delegateInvokeUpdateLayout(launcher.UpdateLayout));
                    }

                    //process
                    {
                        switch (mode)
                        {
                            case FORM_MODE.GDIFORM:
                                {
                                    form = new GdiForm(launcher.game, launcher.FormTickCallBack);
                                    Application.Run((GdiForm)form);
                                    break;
                                }
                            case FORM_MODE.SDLFORM:
                                {
                                    form = new SdlForm(launcher.game, launcher.FormTickCallBack);
                                    ((SdlForm)form).Run();
                                    break;
                                }
                        }
                    }

                    //unlock
                    {
                        launcher.RegisterThreadLauncher(null);
                        launcher.Invoke(new delegateInvokeUpdateLayout(launcher.UpdateLayout));
                    }
                }
                //discollect thread
                {
                    launcher.CollectThread(th, false);
                }
            }
        }
    }
}
