﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Drawing = System.Drawing;

namespace LikeParticle.Content
{
    /// <summary>
    /// GDI(System.Drawing.Graphics)でGameクラスを
    /// 実行・描画するフォーム
    /// </summary>
    public partial class GdiForm : Form, CustomForm.ICustomForm
    {
        PictureBox pixturbox;
        Timer timer;
        Game game;

        CustomForm.OnTickCallback tickCallBack;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="game"></param>
        public static void Run(Game game)
        {
            using (GdiForm f = new GdiForm(game, null))
            {
                Application.Run(f);
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="game"></param>
        public GdiForm(Game game, CustomForm.OnTickCallback tickCallBack)
            : base()
        {
            InitializeComponent();

            //初期化
            {
                this.SuspendLayout();
                this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
                this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
                this.FormBorderStyle = FormBorderStyle.FixedSingle;
                this.MinimizeBox = true;
                this.MaximizeBox = false;
                this.AutoSize = true;
                this.Disposed += new EventHandler(GdiForm_Disposed);

                pixturbox = new PictureBox();
                pixturbox.Location = new Drawing.Point(0, 0);
                pixturbox.Size = new Drawing.Size(800, 600);
                pixturbox.Margin = new Padding(0);
                pixturbox.Image = new Drawing.Bitmap(pixturbox.Size.Width, pixturbox.Size.Height, Drawing.Imaging.PixelFormat.Format32bppArgb);
                this.Controls.Add(pixturbox);
                this.ResumeLayout();
            }

            //stock
            {
                this.game = game;
                this.tickCallBack = tickCallBack;
            }

            //timer
            {
                timer = new Timer();
                timer.Interval = 1000 / 60;
                timer.Tick += new EventHandler(Tick);
                timer.Start();
            }
        }


        /// <summary>
        /// Ticker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Tick(object sender, EventArgs e)
        {
            lock (game)
            {
                game.Update();
                game.Draw(new GdiGraphics(Drawing.Graphics.FromImage(pixturbox.Image)));
                if (tickCallBack != null)
                {
                    tickCallBack(this);
                }
                pixturbox.Invalidate();
            }
        }

        /// <summary>
        /// ウィンドウ ハンドルの取得
        /// </summary>
        /// <returns></returns>
        public IntPtr GetHWND()
        {
            return this.Handle;
        }

        /// <summary>
        /// 描画内容をImageオブジェクトで取得
        /// </summary>
        /// <returns></returns>
        public Drawing.Image GetImage()
        {
            Bitmap img = new Bitmap(pixturbox.Size.Width, pixturbox.Size.Height);
            game.Draw(new GdiGraphics(Drawing.Graphics.FromImage(img)));
            return img;
        }

        /// <summary>
        /// フォームの終了
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void GdiForm_Disposed(object sender, EventArgs e)
        {
            timer.Dispose();
        }
    }
}
