﻿namespace LikeParticle.Content
{
    partial class Launcher
    {
        /// <summary>
        /// 必要なデザイナ変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナで生成されたコード

        /// <summary>
        /// デザイナ サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディタで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGdiForm = new System.Windows.Forms.Button();
            this.buttonSdlForm = new System.Windows.Forms.Button();
            this.pictureBoxMiniMonitor = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMiniMonitor)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonGdiForm
            // 
            this.buttonGdiForm.Location = new System.Drawing.Point(12, 12);
            this.buttonGdiForm.Name = "buttonGdiForm";
            this.buttonGdiForm.Size = new System.Drawing.Size(156, 23);
            this.buttonGdiForm.TabIndex = 0;
            this.buttonGdiForm.Text = "GdiForm";
            this.buttonGdiForm.UseVisualStyleBackColor = true;
            this.buttonGdiForm.Click += new System.EventHandler(this.buttonGdiForm_Click);
            // 
            // buttonSdlForm
            // 
            this.buttonSdlForm.Location = new System.Drawing.Point(12, 50);
            this.buttonSdlForm.Name = "buttonSdlForm";
            this.buttonSdlForm.Size = new System.Drawing.Size(156, 23);
            this.buttonSdlForm.TabIndex = 1;
            this.buttonSdlForm.Text = "SdlForm";
            this.buttonSdlForm.UseVisualStyleBackColor = true;
            this.buttonSdlForm.Click += new System.EventHandler(this.buttonSdlButton_Click);
            // 
            // pictureBoxMiniMoniter
            // 
            this.pictureBoxMiniMonitor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxMiniMonitor.Location = new System.Drawing.Point(8, 88);
            this.pictureBoxMiniMonitor.Name = "pictureBoxMiniMoniter";
            this.pictureBoxMiniMonitor.Size = new System.Drawing.Size(160, 120);
            this.pictureBoxMiniMonitor.TabIndex = 2;
            this.pictureBoxMiniMonitor.TabStop = false;
            // 
            // Launcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(178, 221);
            this.Controls.Add(this.pictureBoxMiniMonitor);
            this.Controls.Add(this.buttonSdlForm);
            this.Controls.Add(this.buttonGdiForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Launcher";
            this.Text = "Launcher";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMiniMonitor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonGdiForm;
        private System.Windows.Forms.Button buttonSdlForm;
        private System.Windows.Forms.PictureBox pictureBoxMiniMonitor;
    }
}