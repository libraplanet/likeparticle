﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Primitives;
using Drawing = System.Drawing;

namespace LikeParticle.Content
{
    /// <summary>
    /// 描画機能をGDI(.NET FrameworkのGraphics)で実装したクラス
    /// </summary>
    public class SdlGraphics : IGraphics
    {
        /// <summary>
        /// Fontオブジェクト
        /// </summary>
        static Font font;

        /// <summary>
        /// 基となるsurface
        /// </summary>
        Surface surface;

        /// <summary>
        /// 静的コンストラクタ
        /// </summary>
        static SdlGraphics()
        {
            font = new Font(Path.Combine(Path.Combine(Environment.GetEnvironmentVariable("SystemRoot"), "fonts"), "MSGOTHIC.TTC"), 16);
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="surface"></param>
        public SdlGraphics(Surface surface)
        {
            this.surface = surface;
        }

        /// <summary>
        /// 文字列の描画
        /// </summary>
        /// <param name="str"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="c"></param>
        public void DrawString(string str, float x, float y, Drawing.Color c)
        {
            surface.Blit(font.Render(str, c), new Drawing.Point((int)x, (int)y));
        }

        /// <summary>
        /// 矩形の塗りつぶしの描画
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <param name="c"></param>
        public void FillRect(float x, float y, float w, float h, Drawing.Color c)
        {
            new Box((short)x, (short)y, (short)(x + w), (short)(y + h)).Draw(surface, c, true, true);
        }

        /// <summary>
        /// 座標を中心に、それぞれの半径の円の描画
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="rw"></param>
        /// <param name="rh"></param>
        /// <param name="c"></param>
        public void FillCircleCenter(float x, float y, float rw, float rh, Drawing.Color c)
        {
            new Ellipse(new Drawing.Point((int)x, (int)y), new Drawing.Size((int)rw, (int)rh)).Draw(surface, c, true, true);
        }

        /// <summary>
        /// 矩形による内接する円の塗りつぶしを描画
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <param name="c"></param>
        public void FillCircleBox(float x, float y, float w, float h, Drawing.Color c)
        {
            FillCircleCenter(x + (w * 0.5f), y + (h * 0.5f), w * 0.5f, h * 0.5f, c);
        }
    }
}
