﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Drawing = System.Drawing;
using LikeParticle.Lib.Common;

namespace LikeParticle.Content
{
    /// <summary>
    /// ゲーム クラス
    /// </summary>
    public class Game : IDisposable
    {
        /// <summary>
        /// 乱数
        /// </summary>
        static Random rand = new Random();

        /// <summary>
        /// 描画のソーティングの可否
        /// </summary>
        static readonly bool DRAWSORT = false;

        /// <summary>
        /// 描画のソーティング時にICompare(.NET Frameworkの機能)
        /// を使うかの可否。
        /// falseでベタなバブルソート
        /// </summary>
        static readonly bool DRAWSORT_COMPARE = true;

        /// <summary>
        /// 円の塗りつぶしをする際に使うメソッドのスイッチ
        ///    true -> IGraphics.FillCircleCenter()
        ///    false -> IGraphics.FillCircleBox()
        /// をコールします。
        /// </summary>
        static readonly bool DRAW_FILLCIRCLECENTER = true;
        
        /// <summary>
        /// パーティクル
        /// </summary>
        class Particle
        {
            /// <summary>
            /// シーケンス管理構造体
            /// </summary>
            struct FRAME_SEQUENCE
            {
                int cnt;
                int max;

                /// <summary>
                /// 初期化
                /// </summary>
                /// <param name="frame"></param>
                public void Init(int frame)
                {
                    cnt = 0;
                    max = frame;
                }

                /// <summary>
                /// 更新
                /// </summary>
                public void Update()
                {
                    if (cnt < max)
                    {
                        cnt++;
                    }
                    else
                    {
                        cnt = max;
                    }
                }

                /// <summary>
                /// 終了の可否の取得
                /// </summary>
                /// <returns></returns>
                public bool IsEnd()
                {
                    return (cnt == max);
                }

                /// <summary>
                /// シーケンスに対応するfloat値を、
                /// 指定された値から計算して取得します。
                /// </summary>
                /// <param name="f"></param>
                /// <returns></returns>
                public float GetSeeqValue(float f)
                {
                    if (max == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        return ((f * ((float)cnt)) / ((float)max));
                    }
                }

                /// <summary>
                /// シーケンスに対応するint値を、
                /// 指定された値から計算して取得します。
                /// </summary>
                /// <param name="n"></param>
                /// <returns></returns>
                public int GetSeeqValue(int n)
                {
                    if (max == 0)
                    {
                        return 0;
                    }
                    else
                    {
                        return ((n * cnt) / max);
                    }
                }

                /// <summary>
                /// シーケンスに対応するbyte値を、
                /// 指定された値から計算して取得します。
                /// </summary>
                /// <param name="n"></param>
                /// <returns></returns>
                public byte GetSeeqValue(byte n)
                {
                    return (byte)GetSeeqValue((int)n);
                }
            }
            
            /// <summary>
            /// 座標
            /// </summary>
            Drawing.PointF pos = new System.Drawing.PointF();

            /// <summary>
            /// 大きさ
            /// </summary>
            float size = 0.0f;

            /// <summary>
            /// 色
            /// </summary>
            Drawing.Color col = Drawing.Color.FromArgb(0, 0, 0);

            /// <summary>
            /// activity
            /// </summary>
            bool active = false;

            /// <summary>
            /// visible
            /// </summary>
            bool visible = false;

            /// <summary>
            /// 総合的なシーケンスの管理
            /// </summary>
            int task = 0;
            
            /// <summary>
            /// サイズ変化のシーケンス管理
            /// </summary>
            FRAME_SEQUENCE seq_size;
            
            /// <summary>
            /// アルファ変化のシーケンス管理
            /// </summary>
            FRAME_SEQUENCE seq_alpha;
            
            /// <summary>
            /// インターバル シーケンス管理
            /// (透明になってからリセット(再設定)されるまでの区間)
            /// </summary>
            FRAME_SEQUENCE seq_interval;

            /// <summary>
            /// debug用シリアル番号
            /// (配列の要素番号入れたりとか・・・)
            /// </summary>
            int serial;

            /// <summary>
            /// コンストラクタ
            /// </summary>
            /// <param name="serial"></param>
            public Particle(int serial)
            {
                this.serial = serial;
            }

            /// <summary>
            /// パラメータの一括セット
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <param name="size"></param>
            /// <param name="size_frame"></param>
            /// <param name="alpha_frame"></param>
            /// <param name="interval_frame"></param>
            /// <param name="red"></param>
            /// <param name="geen"></param>
            /// <param name="blue"></param>
            public void SetParameter(float x, float y, float size, int size_frame, int alpha_frame, int interval_frame, int red, int geen, int blue)
            {
                pos = new Drawing.PointF(x, y);
                this.size = size;
                col = Drawing.Color.FromArgb(red, geen, blue);
                seq_size.Init(size_frame);
                seq_alpha.Init(alpha_frame);
                seq_interval.Init(interval_frame);

                //Debug.WriteLine(
                //    "[Game.Particel.SetParameter()]"
                //    + " x(" + x.ToString("000") + ")"
                //    + " y(" + y.ToString("000") + ")"
                //    + " size(" + size.ToString("000.0") + ")"
                //    + " size_frame(" + size_frame.ToString("000") + ")"
                //    + " alpha_frame(" + alpha_frame.ToString("000") + ")"
                //    + " interval_frame(" + interval_frame.ToString("000") + ")"
                //    + " red(" + red.ToString("000") + ")"
                //    + " geen(" + geen.ToString("000") + ")"
                //    + " blue(" + blue.ToString("000") + ")"
                //);

                task = 0;
                visible = true;
            }

            /// <summary>
            /// 開始
            /// </summary>
            public void Start()
            {
                active = true;
                Reset();
            }

            /// <summary>
            /// リセット
            /// </summary>
            public void Reset()
            {
                SetParameter(
                        rand.Next(20, 800 - 20),
                        rand.Next(20, 600 - 20),
                        ((float)rand.Next(100, 3000)) / 10.0f, //10倍のスケーリングで指定
                        rand.Next(30, 120),
                        rand.Next(30, 120),
                        rand.Next(300, 600),
                        rand.Next(0, 255),
                        rand.Next(0, 255),
                        rand.Next(0, 255)
                    );
            }

            /// <summary>
            /// Debug用シリアル
            /// </summary>
            public int Serial
            {
                get
                {
                    return serial;
                }
            }

            /// <summary>
            /// 描画サイズの取得
            /// </summary>
            public float Size
            {
                get
                {
                    return seq_size.GetSeeqValue(size);
                }
            }

            /// <summary>
            /// 描画用矩形の取得
            /// </summary>
            public Drawing.RectangleF Rect
            {
                get
                {
                    float s = Size;
                    return new Drawing.RectangleF(
                        pos.X - (s * 0.5f),
                        pos.Y - (s * 0.5f),
                        s,
                        s
                    );
                }
            }

            /// <summary>
            /// 色の取得
            /// </summary>
            public Drawing.Color Color
            {
                get
                {
                    return Drawing.Color.FromArgb(
                        255 - seq_alpha.GetSeeqValue(col.A),
                        col
                    );
                }
            }

            /// <summary>
            /// 描画可否の取得
            /// </summary>
            /// <returns></returns>
            public bool IsDrawable()
            {
                return (active & visible);
            }

            /// <summary>
            /// 更新
            /// </summary>
            public void Update()
            {
                if (task == 0)
                {
                    //サイズ変化アニメーション
                    if (!seq_size.IsEnd())
                    {
                        seq_size.Update();
                    }
                    else
                    {
                        task++;
                    }
                }
                else if (task == 1)
                {
                    //アルファ アニメーション
                    if (!seq_alpha.IsEnd())
                    {
                        seq_alpha.Update();
                    }
                    else
                    {
                        visible = false;
                        task++;
                    }
                }
                else if (task == 2)
                {
                    //インターバル
                    if (!seq_interval.IsEnd())
                    {
                        seq_interval.Update();
                    }
                    else
                    {
                        Reset();
                    }
                }
            }

            /// <summary>
            /// 描画
            /// </summary>
            /// <param name="g"></param>
            public void Draw(IGraphics g)
            {
                if (IsDrawable())
                {
                    if (DRAW_FILLCIRCLECENTER)
                    {
                        float size = Size * 0.5f;
                        g.FillCircleCenter(pos.X, pos.Y, size, size, Color);
                    }
                    else
                    {
                        Drawing.RectangleF r = Rect;
                        g.FillCircleBox(r.X, r.Y, r.Width, r.Height, Color);
                    }
                }
            }
        }

        /// <summary>
        /// 独自ソート
        /// </summary>
        class ParticleSorter : IComparer<Particle>
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="a"></param>
            /// <param name="b"></param>
            /// <returns></returns>
            public int Compare(Particle a, Particle b)
            {
                return (int)(a.Size - b.Size);
            }
       }

        /// <summary>
        /// FPSを管理
        /// </summary>
        FpsCounter fpsManager = new FpsCounter(10);

        /// <summary>
        /// particle変数
        /// </summary>
        Particle[] particles = new Particle[128];

        /// <summary>
        /// particle追加シーケンス
        /// </summary>
        int[] seq = {0, 0};

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Game()
        {
            for (int i = 0; i < particles.Length; i++)
            {
                particles[i] = new Particle(i);
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        public void Update()
        {
            fpsManager.Update();

            //追加
            {
                if (seq[0] < particles.Length)
                {
                    if (seq[1]++ == 0)
                    {
                        particles[seq[0]].Start();
                        seq[0]++;
                    }
                    seq[1] %= 4;
                }
            }
            //更新
            {
                foreach (Particle p in particles)
                {
                    if (p != null)
                    {
                        p.Update();
                    }
                }

            }
        }

        /// <summary>
        /// 描画
        /// </summary>
        /// <param name="g"></param>
        public void Draw(IGraphics g)
        {
            //背景のクリア
            g.FillRect(0, 0, 800, 600, Drawing.Color.FromArgb(255, 255, 255));
            //パーティクルの描画
            {
                Particle[] list = (Particle[])particles.Clone();
                //ソーティング
                {
                    if(DRAWSORT)
                    {
                        if(DRAWSORT_COMPARE)
                        {
                            //ICompareによるソート
                            Array.Sort(list, new ParticleSorter());
                            //for (int i = 0; i < particles.Length; i++)
                            //{
                            //    Debug.WriteLine("[" + i + "] (" + particles[i].Serial.ToString("0000") + ", " + list[i].Serial.ToString("0000") + ")");
                            //}
                        }
                        else 
                        {
                            //適当なバブルソート
                            for (int i = 0; i < list.Length - 1; i++)
                            {
                                for (int j = i + 1; j < list.Length; j++)
                                {
                                    if (list[i].Size > list[j].Size)
                                    {
                                        Particle t = list[i];
                                        list[i] = list[j];
                                        list[j] = t;

                                    }
                                }
                            }
                        }
                   }
                }
                //描画
                {
                    for(int i = 0; i < list.Length; i++)
                    {
                        Particle p = list[i];
                        if (p != null)
                        {
                            //Debug.WriteLine("[Game.Draw()] size(" + p.Size.ToString("000.0000") + ")");
                            p.Draw(g);
                        }
                    }
                    //Debug.WriteLine("");
                }
            }
            //文字を描画
            {
                //縁取りのを含めて計9回書く
                //  [0][1][2]    [4][5][6]
                //  [3][4][5] -> [7][8][0]
                //  [6][7][8]    [1][2][3]
                //の順番に記述します。
                for (int j = 0; j < 9; j++)
                {
                    Drawing.Color c;
                    int k = (j + 5) % 9;
                    int x = k % 3;
                    int y = k / 3;
                    if (k == 4)
                    {
                        c = Drawing.Color.FromArgb(255, 255, 255);
                    }
                    else
                    {
                        c = Drawing.Color.FromArgb(102, 102, 102);
                    }
                    //パーティクルの数の描画
                    {
                        int n = 0;
                        foreach (Particle p in particles)
                        {
                            if(p.IsDrawable())
                            {
                                n++;
                            }
                        }
                        g.DrawString(
                            "particles(" + n.ToString("000") + ") ",
                            x + 10,
                            y + 10,
                            c
                        );
                    }
                    //fpsを表示
                    {
                        for (int i = 0; i < fpsManager.Size; i++)
                        {
                            g.DrawString(
                                "fps[" + i.ToString("00") + "] " + fpsManager.GetFPS(i).ToString("000.000"),
                                x + 10,
                                y + 26 + (i * 16),
                                c
                            );
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
        }
    }
}
