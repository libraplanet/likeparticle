﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace LikeParticle.Content
{
    /// <summary>
    /// 描画抽象化interface
    /// </summary>
    public interface IGraphics
    {
        /// <summary>
        /// 文字列の描画
        /// </summary>
        /// <param name="str"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="c"></param>
        void DrawString(string str, float x, float y, Color c);

        /// <summary>
        /// 矩形の塗りつぶしの描画
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <param name="c"></param>
        void FillRect(float x, float y, float w, float h, Color c);

        /// <summary>
        /// 座標を中心に、それぞれの半径の円の描画
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="rw"></param>
        /// <param name="rh"></param>
        /// <param name="c"></param>
        void FillCircleCenter(float x, float y, float rw, float rh, Color c);

        /// <summary>
        /// 矩形による内接する円の塗りつぶしを描画
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <param name="c"></param>
        void FillCircleBox(float x, float y, float w, float h, Color c);
    }
}
