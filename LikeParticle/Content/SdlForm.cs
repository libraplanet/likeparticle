﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Text;
using SdlDotNet;
using SdlDotNet.Core;
using SdlDotNet.Windows;
using SdlDotNet.Graphics;
using Drawing = System.Drawing;

namespace LikeParticle.Content
{
    /// <summary>
    /// SDLでGameクラスを
    /// 実行・描画するフォーム
    /// (フォーム部分もSDLのVideoModeを使用)
    /// </summary>
    public class SdlForm : IDisposable, CustomForm.ICustomForm
    {
        Game game;
        Surface surface;
        CustomForm.OnTickCallback tickCallBack;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="game"></param>
        public static void Run(Game game)
        {
            using(SdlForm f = new SdlForm(game, null))
            {
                f.Run();
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="game"></param>
        public SdlForm(Game game, CustomForm.OnTickCallback tickCallBack)
        {
            //SDL Setting
            {
                //video setting
                {
                    if (System.Environment.OSVersion.Platform == PlatformID.Win32NT)
                    {
                        System.Environment.SetEnvironmentVariable("SDL_VIDEODRIVER", "directx");
                    }
                }

                this.surface = Video.SetVideoMode(800, 600, false, false, false);
                Video.WindowCaption = "SdlForm";
                Events.TargetFps = 60;
                Events.Tick += new EventHandler<TickEventArgs>(Tick);
                Events.Quit += new EventHandler<QuitEventArgs>(Quit);

                //debug out
                {
                    Debug.WriteLine("[SdlForm.SdlForm()] Using Video Driver (" + Video.VideoDriver + ")");
                    Debug.WriteLine("[SdlForm.SdlForm()] Hardware Surface   (" + VideoInfo.HasHardwareSurfaces.ToString() + ")");
                    Debug.WriteLine("[SdlForm.SdlForm()] Hardware Blits     (" + VideoInfo.HasHardwareBlits.ToString() + ")");
                }
            }

            //setting instance
            {
                this.game = game;
                this.tickCallBack = tickCallBack;
            }
        }

        /// <summary>
        /// ウィンド ウハンドルの取得
        /// </summary>
        /// <returns></returns>
        public IntPtr GetHWND()
        {
            return Video.WindowHandle;
        }

        /// <summary>
        /// 描画内容をImageオブジェクトで取得
        /// </summary>
        /// <returns></returns>
        public Drawing.Image GetImage()
        {
            Surface suf = new Surface(Video.Screen.Size);
            game.Draw(new SdlGraphics(suf));
            return suf.Bitmap;
        }

        /// <summary>
        /// 実行
        /// </summary>
        public void Run()
        {
            Events.Run();
        }

        /// <summary>
        /// Ticker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Tick(object sender, TickEventArgs e)
        {
            lock (game)
            {
                game.Update();
                game.Draw(new SdlGraphics(surface));
                if (tickCallBack != null)
                {
                    tickCallBack(this);
                }
                surface.Update();
            }
        }

        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Quit(object sender, QuitEventArgs e)
        {
            Dispose();
        }

        /// <summary>
        /// 終了
        /// </summary>
        public void Close()
        {
            Dispose();
        }

        /// <summary>
        /// 破棄
        /// </summary>
        public void Dispose()
        {
            Events.QuitApplication();
        }
    }
}
