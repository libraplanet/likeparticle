﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace LikeParticle.Content
{
    /// <summary>
    /// CustomFormクラス
    /// interface内でdelgateを定義出来ないと言うので、
    /// 名前空間的な意味合いで存在する、中身の無いクラス
    /// </summary>
    public static class CustomForm
    {
        /// <summary>
        /// Tickイベントのコールバック
        /// Tick中に、ホスト(この場合はLauncherクラス)で行いたいイベント用に、
        /// 使ってコールバックする為のdelegate。
        /// ICustomFormを実装したクラスで引数として受け取って、
        /// Tickイベント中の任意の場所からコールしてあげる。
        /// 引数のformには自身を指定する。
        /// </summary>
        /// <param name="form"></param>
        public delegate void OnTickCallback(ICustomForm form);

        /// <summary>
        /// Formの抽象化interface。
        /// 共通のメソッドを抽象化して、実装必須とします。
        /// </summary>
        public interface ICustomForm
        {
            /// <summary>
            /// ウィンドウ ハンドルの取得
            /// </summary>
            /// <returns></returns>
            IntPtr GetHWND();

            /// <summary>
            /// 描画しているGame内容、自身のレンダリング方法(IGraphics)を使って描画した
            /// 現在のGame内容(≒フレームバッファ)をImageオブジェクトとして取得します。
            /// </summary>
            /// <returns></returns>
            Image GetImage();

            /// <summary>
            /// フォームの終了
            /// </summary>
            void Close();
        }
    }
}
