﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Drawing = System.Drawing;

using LikeParticle.Content;
using LikeParticle.Lib;

namespace LikeParticle
{
    class Program
    {
        /// <summary>
        /// エントリポイント
        /// </summary>
        [STAThread]
        public static void Main()
        {
            using (Game game = new Game())
            {
                //GdiForm.Run(game);
                //SdlForm.Run(game);
                using (Form f = new Launcher(game))
                {
                    Application.Run(f);
                }
            }
        }
    }
}
