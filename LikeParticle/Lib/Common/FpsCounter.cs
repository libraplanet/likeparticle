﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LikeParticle.Lib.Common
{
    /// <summary>
    /// FPS カウント クラス
    /// 集計されたFPSをロギングし、さかのぼって取得を可能にするI/Fを提供します。
    /// 取得の際は、基本カレントを0とし、指定された数だけ過去にさかのぼって返します。
    /// </summary>
    public class FpsCounter
    {
        int count;
        double lastTime;
        double[] log_fps;
        double[] log_dist;
        double[] log_time;
        int[] log_count;

        /// <summary>
        /// コンストラクタ
        /// ロギング可能なサイズ(回数)をしてします。
        /// </summary>
        /// <param name="size"></param>
        public FpsCounter(int size)
        {
            count = 0;
            lastTime = EnhancedTimer.GetMillisecondsDouble();
            log_fps = new double[size];
            log_dist = new double[size];
            log_time = new double[size];
            log_count = new int[size];
        }

        /// <summary>
        /// FPSをログから取得します。
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public double GetFPS(int i)
        {
            return log_fps[i];
        }

        /// <summary>
        /// ログが更新されたときの、前のログとの時間差をログから取得します。
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public double GetLogDist(int i)
        {
            return log_dist[i];
        }

        /// <summary>
        /// ログが更新されたときの時間をログから取得します。
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public double GetLogTime(int i)
        {
            return log_time[i];
        }

        /// <summary>
        /// カウントされた回数をログから取得します。
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public float GetLogCount(int i)
        {
            return log_count[i];
        }

        /// <summary>
        /// ログサイズの取得。
        /// </summary>
        public int Size
        {
            get
            {
                return log_fps.Length;
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        public void Update()
        {
            double time = EnhancedTimer.GetMillisecondsDouble();
            double dist = time - lastTime;
            if (dist < 1000)
            {
                count++;
            }
            else
            {
                Array.Copy(log_fps, 0, log_fps, 1, log_fps.Length - 1);
                log_fps[0] = (float)count / (float)dist * 1000.0f;

                Array.Copy(log_dist, 0, log_dist, 1, log_dist.Length - 1);
                log_dist[0] = dist;
                Array.Copy(log_time, 0, log_time, 1, log_time.Length - 1);
                log_time[0] = time;
                Array.Copy(log_count, 0, log_count, 1, log_count.Length - 1);
                log_count[0] = count;

                count = 0;
                lastTime = time;
            }
        }
    }
}
