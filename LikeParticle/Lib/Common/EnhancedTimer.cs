﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LikeParticle.Lib.Common
{
    /// <summary>
    /// 高精度タイマー
    /// システムのタイマーだと解像度が悪く制度が出にくいので、
    /// その辺を吸収する抽象化されたクラス。
    /// </summary>
    class EnhancedTimer
    {
        static Stopwatch sw = new Stopwatch();

        /// <summary>
        /// 静的コンストラクタ
        /// </summary>
        static EnhancedTimer()
        {
            sw.Start();
        }

        /// <summary>
        /// Milli Secondsをlongで取得
        /// </summary>
        /// <returns></returns>
        public static long GetMillisecondsLong()
        {
            return sw.ElapsedMilliseconds;
        }

        /// <summary>
        /// Milli Secondsをdoubleで取得
        /// </summary>
        /// <returns></returns>
        public static double GetMillisecondsDouble()
        {
            return GetSecondsDouble() * (double)1000;
        }

        /// <summary>
        /// Secondsをdoubleで取得
        /// </summary>
        /// <returns></returns>
        public static double GetSecondsDouble()
        {
            return (double)sw.ElapsedTicks / (double)Stopwatch.Frequency;
        }
    }
}
