LikeParticle
	090919		takumi

◆概要
	LikePirticle
	Pirticleの様なもの(Like)を表示する、C#のサンプルコードみたいなものです。
	矩形と円を描画しているだけなので、ほんとにPirticleではありません(苦笑)。
	GDI(System.Drawing。Graphics)とSDLの比較が若干楽しめます。
	まぁ殆ど同じですけど。

◆環境
	Visual C# 2008 Express
	.NET Framework 2.0
	SDL.NET(6.1で確認)

◆使う前に
	SDL.NETのライブラリとランタイムが必要になります。
	ライセンスの関係上、同梱していません。
	その為、SDL.NETを入手して配置する必要があります。

		サイト			：	http://cs-sdl.sourceforge.net/index.php/Main_Page
		DLするファイル	：	sdldotnet-6.1.0.zip
		配置元			；	sdldotnet-6.1.0.zip の ./sdldotnet-6.1.0/
		配置先			；	./lib/sdldotnet/
		【レイアウト】
			[./]
				├[lib]
				｜		└[sdldotnet]
				｜				├[bin]
				｜				├[lib]
				｜				├・・・
				｜				└・・・
				｜
				├[LikeParticle]
				｜		├・・・
				｜		├LikeParticle.csproj
				｜		├LikeParticle.csproj.user
				｜		├・・・
				｜		└・・・
				｜
				├[scrap]
				｜		├・・・
				｜		└・・・
				｜
				├launch_solution.bat
				├launch_solution.sln
				├launch_solution.suo
				├pack.bat
				└readme.txt

	手順01)
		SDL.NETのサイトのDownloadからsdldotnet-6.1.0.zipをDLする。
		sdldotnet-6.1.0-runtime-setup.exeやsdldotnet-6.1.0-sdk-setup.exeは別に要らない。

		http://sourceforge.net/projects/cs-sdl/files/SDL.NET/6.1.0/sdldotnet-6.1.0.zip/download

	手順02)
		落としてきたsdldotnet-6.1.0.zipを適当に解凍する。

	手順03)
		トップレベルのフォルダのバージョン部分だけ削除してリネームする。
		sdldotnet-6.1.0 -> sdldotnet
		(なんでトップレベルのフォルダをZIPに含めるかなぁ・・・)

	手順04)
		解凍してあるLikeParticleのプロジェクト(厳密にはソリューション) フォルダのルートに
		libフォルダを作成する。

	手順05)
		手順04で作ったlibフォルダに、手順03のsdldotnetフォルダをコピーする。

	階層が、上記レイアウトの様になればOK。

◆使い方
	launch_solution.batからLikeParticle.slnを起動する
	(ダブルクリック、もしくは引数無しでbatを実行)。

	LikeParticle.slnから直にVisual Studioを起動させたり、
	既に起動中のVisual StudioにLikeParticle.slnを読み込ませたりしてはいけない。
	launch_solution.batで、SDLのランタイム フォルダにPATHを通してVisual Studioを起動して実行しているため、
	ビルドは出来るが起動が出来ない。

	詳しくはlaunch_solution.batを見れば良いけど、たいした事してないよ。
	既にランタイムがインストール済みで、そこにPATHが通っているなら、batが無くても起動できるけど。

	その為、出来たバイナリも、PATHが通っている状態にするか、
	sdldotnet-6.1.0.zipの中のbinの中のdllなどを実行ファイルのカレントにコピーするかしないと起動することが出来ない。

◆その他
	Threadを使ってフォームを切り替えているので、若干動作がおかしかったり不安定だったりする。
	また、Win32APIを使っている場所もあるので、Linuxとかでmonoを使って動かすのもダメなんじゃないかな。

	このソリューションを含め、全てにおける不具合に関する責任は負いかねます。

◇そんなところ。
